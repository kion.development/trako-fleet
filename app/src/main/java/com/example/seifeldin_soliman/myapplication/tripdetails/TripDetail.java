package com.example.seifeldin_soliman.myapplication.tripdetails;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by seifeldin_soliman on 11/25/2018.
 */

class TripDetail implements Serializable {

    @SerializedName("userId")
    private String userId;
    @SerializedName("tripId")
    private String tripId;
    @SerializedName("driverName")
    private String driverName;
    @SerializedName("vehiclePlateNumber")
    private String vehiclePlateNumber;
    @SerializedName("startPoint")
    private String startPoint;
    @SerializedName("endPoint")
    private String endPoint;
    @SerializedName("vehicleSpeedLimit")
    private String vehicleSpeedLimit;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getTripId() {
        return tripId;
    }

    public void setTripId(String tripId) {
        this.tripId = tripId;
    }

    public String getDriverName() {
        return driverName;
    }

    public void setDriverName(String driverName) {
        this.driverName = driverName;
    }

    public String getVehiclePlateNumber() {
        return vehiclePlateNumber;
    }

    public void setVehiclePlateNumber(String vehiclePlateNumber) {
        this.vehiclePlateNumber = vehiclePlateNumber;
    }

    public String getStartPoint() {
        return startPoint;
    }

    public void setStartPoint(String startPoint) {
        this.startPoint = startPoint;
    }

    public String getEndPoint() {
        return endPoint;
    }

    public void setEndPoint(String endPoint) {
        this.endPoint = endPoint;
    }

    public String getVehicleSpeedLimit() {
        return vehicleSpeedLimit;
    }

    public void setVehicleSpeedLimit(String vehicleSpeedLimit) {
        this.vehicleSpeedLimit = vehicleSpeedLimit;
    }


}
