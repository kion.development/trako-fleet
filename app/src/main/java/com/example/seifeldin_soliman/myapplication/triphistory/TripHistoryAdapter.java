package com.example.seifeldin_soliman.myapplication.triphistory;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.seifeldin_soliman.myapplication.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by seifeldin_soliman on 11/29/2018.
 */

public class TripHistoryAdapter extends RecyclerView.Adapter<TripHistoryAdapter.MyViewHolder> {

    public List<TripHistory> tripHistorylist;


    public TripHistoryAdapter(List<TripHistory> tripHistory) {
        this.tripHistorylist = tripHistory;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.histroy_trip_recycler_item, parent, false);
        MyViewHolder holder = new MyViewHolder(itemView);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {

        TripHistory tripHistory = tripHistorylist.get(position);
        holder.statusMessage.setText(tripHistory.getStatus());
        holder.time.setText(tripHistory.getTime());


    }

    @Override
    public int getItemCount() {
        return tripHistorylist.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView statusMessage, time;

        public MyViewHolder(View itemView) {
            super(itemView);
            statusMessage = itemView.findViewById(R.id.message_status_tv);
            time = itemView.findViewById(R.id.time_tv);
        }
    }
}
