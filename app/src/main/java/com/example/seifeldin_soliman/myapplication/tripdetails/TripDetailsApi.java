package com.example.seifeldin_soliman.myapplication.tripdetails;

import com.example.seifeldin_soliman.myapplication.login.UserLoginModel;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;

/**
 * Created by seifeldin_soliman on 11/8/2018.
 */

public interface TripDetailsApi {
    @GET("trips/{trip_id}/details")
    Call<TripDetailsModel> getTripdetails(@Path("trip_id") String trip_id);


}
