package com.example.seifeldin_soliman.myapplication.triphistory;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.MenuItem;

import com.example.seifeldin_soliman.myapplication.R;
import com.example.seifeldin_soliman.myapplication.RetrofitInstance;
import com.example.seifeldin_soliman.myapplication.tripdetails.DetailsActivity;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class TripHistoryActivity extends AppCompatActivity {

    SharedPreferences getSharedPref;
    private RecyclerView recyclerView;
    private TripHistoryAdapter tripHistoryAdapter;
    private ArrayList<TripHistory> tripHistoriesList;
    private String tripId;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.history_trip_activity);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        recyclerView = (RecyclerView) findViewById(R.id.history_trip_rv);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        getSharedPref = getApplication().getSharedPreferences("MyPrefs", Context.MODE_PRIVATE);
        Intent intent = getIntent();

        tripId = intent.getStringExtra("tripId");
        doConnection(tripId);
    }

    public void doConnection(String tripId) {

//        String userId =  getSharedPref.getString("userId", "0");

        Retrofit retrofit = RetrofitInstance.retrofitConnection();

        TripHistoryAPI tripHistoryAPI = retrofit.create(TripHistoryAPI.class);

        Call<GetTripHistoryModel> getTripHistoryModelCall = tripHistoryAPI.getTripHistoryInfo(tripId);

        getTripHistoryModelCall.enqueue(new Callback<GetTripHistoryModel>() {
            @Override
            public void onResponse(Call<GetTripHistoryModel> call, Response<GetTripHistoryModel> response) {

                if (response.isSuccessful() && response != null) {

                    Log.d("DailyTrips", "onResponse: ");
                    String code = response.body().status.getCode();

                    if (code.equals("200")) {

                        tripHistoriesList = response.body().tripHistory;
                        tripHistoryAdapter = new TripHistoryAdapter(tripHistoriesList);
                        recyclerView.setAdapter(tripHistoryAdapter);

                    } else if (code.equals("404")) {
                        Snackbar.make(findViewById(R.id.trip_history_activity), "Trip Not Found", Snackbar.LENGTH_SHORT).show();
                    }
                } else {
                    Snackbar.make(findViewById(R.id.trip_history_activity), "خطأ فى استرداد البيانات", Snackbar.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onFailure(Call<GetTripHistoryModel> call, Throwable t) {
                Snackbar.make(findViewById(R.id.trip_history_activity), "عفوا فشل فى تحميل البيانات , برجاء التاكد من جودة الانترنت", Snackbar.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                Intent tripHistoryIntent = new Intent(TripHistoryActivity.this, DetailsActivity.class);
                tripHistoryIntent.putExtra("tripId",tripId);
                startActivity(tripHistoryIntent);
                this.finish();
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
