package com.example.seifeldin_soliman.myapplication.triphistory;

import com.google.gson.annotations.SerializedName;

/**
 * Created by seifeldin_soliman on 12/2/2018.
 */

public class TripHistory {

    @SerializedName("Status")
    String status;
    @SerializedName("Time")
    String Time;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getTime() {
        return Time;
    }

    public void setTime(String time) {
        Time = time;
    }
}
