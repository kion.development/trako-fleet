package com.example.seifeldin_soliman.myapplication.dailytrips;

import com.example.seifeldin_soliman.myapplication.Status;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by seifeldin_soliman on 12/2/2018.
 */

public class GetDailyTripModel {

    @SerializedName("Status")
    Status status;
    @SerializedName("Trips")
    ArrayList<Trips> dailyTripsList;

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public ArrayList<Trips> getDailyTripsList() {
        return dailyTripsList;
    }

    public void setDailyTripsList(ArrayList<Trips> dailyTripsList) {
        this.dailyTripsList = dailyTripsList;
    }
}
