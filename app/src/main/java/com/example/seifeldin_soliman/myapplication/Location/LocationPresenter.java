package com.example.seifeldin_soliman.myapplication.Location;

import android.app.Activity;
import android.content.Intent;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by seifeldin_soliman on 12/26/2018.
 */

public class LocationPresenter extends FirebaseMessagingService {


    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        super.onMessageReceived(remoteMessage);

        if (remoteMessage.getData().size() > 0) {
            Log.d("FCM", "onMessageReceived: " + remoteMessage.getData());

            JSONObject receivedData = new JSONObject(remoteMessage.getData());
            String lat = "";
            String longit = "";
            try {
                lat = receivedData.getString("latitude");
                longit = receivedData.getString("longitude");

                LocalBroadcastManager broadcaster = LocalBroadcastManager.getInstance(getBaseContext());

                Intent intent = new Intent("REQUEST_ACCEPT");
                intent.putExtra("lat", lat);
                intent.putExtra("long", longit);
                broadcaster.sendBroadcast(intent);


            } catch (JSONException e) {
                e.printStackTrace();
            }

        }

        if (remoteMessage.getNotification() != null) {
            Log.d("FCM", "onMessageReceivedNotification: " + remoteMessage.getNotification().getBody());
        }
    }

}
