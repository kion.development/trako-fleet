package com.example.seifeldin_soliman.myapplication.triphistory;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

/**
 * Created by seifeldin_soliman on 12/2/2018.
 */

public interface TripHistoryAPI  {


    @GET("trips/{trip_id}/history")
    Call<GetTripHistoryModel> getTripHistoryInfo(@Path("trip_id")String id);
}
