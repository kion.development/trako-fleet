package com.example.seifeldin_soliman.myapplication.login;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import com.example.seifeldin_soliman.myapplication.R;
import com.example.seifeldin_soliman.myapplication.RetrofitInstance;
import com.example.seifeldin_soliman.myapplication.dailytrips.DailyTripsActivity;
import com.google.firebase.iid.FirebaseInstanceId;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class LoginActivity extends AppCompatActivity {

    TextInputEditText userName, password;
    TextInputLayout email;
    Button loginBtn;
    SharedPreferences userDataShredPref;
    SharedPreferences.Editor editor;
    String userData = "USER_DATA";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        userName = findViewById(R.id.email);
        email = findViewById(R.id.email_ti);
        password = findViewById(R.id.password);
        loginBtn = (Button) findViewById(R.id.btn_login);


        userDataShredPref = getSharedPreferences(userData, Context.MODE_PRIVATE);
        editor = userDataShredPref.edit();
        loginBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String token = FirebaseInstanceId.getInstance().getToken();
                Log.d("Token", "onCreate: " + token);
                String email = userName.getText().toString();
                String pass = password.getText().toString();
                if (TextUtils.isEmpty(email)) {
                    userName.setError("من فضلك ادخل الاسم");
                } else if (TextUtils.isEmpty(pass)) {
                    password.setError("من فضلك ادخل كلمة السر");
                } else {
                    getDriverInfo(email, pass, token);
                }

            }
        });
    }

    public void getDriverInfo(String email, String password, String token) {

        Retrofit retrofit = RetrofitInstance.retrofitConnection();
        LoginApi branchesAPI = retrofit.create(LoginApi.class);

        Call<UserLoginModel> getParentInfoModelCall = branchesAPI.getTripInfo(email, password, token);

        getParentInfoModelCall.enqueue(new Callback<UserLoginModel>() {
            @Override
            public void onResponse(@NonNull Call<UserLoginModel> call, @NonNull Response<UserLoginModel> response) {

                if (response.isSuccessful() && response.body() != null) {

                    Log.d(" TripDetails", "onResponse: " + response);
                    String code = response.body().getStatus().getCode();
                    User userData = response.body().getUser();
                    if (code.equals("200")) {

                        Intent intent = new Intent(LoginActivity.this, DailyTripsActivity.class);

//                        intent.putExtra("tripDetail", (Serializable) tripDetail);

                        editor.putString("userId", userData.getUserId());
                        editor.commit();

                        startActivity(intent);

                    } else if (code.equals("7001")) {
                        Snackbar.make(findViewById(R.id.Login_activity), "Not Correct Email", Snackbar.LENGTH_SHORT).show();
                    } else if (code.equals("7003")) {

                        Snackbar.make(findViewById(R.id.Login_activity), "Wrong, Email or password Not Found In Our Database", Snackbar.LENGTH_SHORT).show();
                    } else if (code.equals("7008")) {

                        Snackbar.make(findViewById(R.id.Login_activity), "Sorry, The company is deactivated", Snackbar.LENGTH_SHORT).show();
                    } else {
                        Snackbar.make(findViewById(R.id.Login_activity), "Internal Error", Snackbar.LENGTH_SHORT).show();
                    }
                } else {
                    Snackbar.make(findViewById(R.id.Login_activity), "General Error", Snackbar.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<UserLoginModel> call, Throwable t) {
                Snackbar.make(findViewById(R.id.Login_activity), "Oops! Failed to download data, please check your internet connection", Snackbar.LENGTH_SHORT).show();
            }
        });
    }

}
