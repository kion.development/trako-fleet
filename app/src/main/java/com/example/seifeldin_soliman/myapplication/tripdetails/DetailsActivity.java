package com.example.seifeldin_soliman.myapplication.tripdetails;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;

import com.example.seifeldin_soliman.myapplication.Location.MapsActivity;
import com.example.seifeldin_soliman.myapplication.R;
import com.example.seifeldin_soliman.myapplication.RetrofitInstance;
import com.example.seifeldin_soliman.myapplication.dailytrips.DailyTripsAPI;
import com.example.seifeldin_soliman.myapplication.dailytrips.DailyTripsActivity;
import com.example.seifeldin_soliman.myapplication.dailytrips.DailyTripsAdapter;
import com.example.seifeldin_soliman.myapplication.dailytrips.GetDailyTripModel;
import com.example.seifeldin_soliman.myapplication.dailytrips.Trips;
import com.example.seifeldin_soliman.myapplication.triphistory.TripHistoryActivity;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class DetailsActivity extends AppCompatActivity {

    private TextView busDriver, vehiclePlateNumber, startPoint, endPoint, vehicleSpeedLimit;
    private TripDetail tripDetail;
    private Toolbar toolbar;
    private ImageButton notificationBt, menuBt, backArrow;
    private FloatingActionButton floatingActionButton;
    private String tripId;
    private SharedPreferences getUserData;
    private String userId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details);
        initView();
        getUserData = getApplication().getSharedPreferences("USER_DATA", Context.MODE_PRIVATE);
        userId = getUserData.getString("userId", "0");
        Intent intent = getIntent();
        tripId = intent.getStringExtra("tripId");
        //TODO: get id .
        notificationBt.setOnClickListener(v -> {
            Intent tripHistoryIntent = new Intent(DetailsActivity.this, TripHistoryActivity.class);
            tripHistoryIntent.putExtra("tripId",tripId);
            startActivity(tripHistoryIntent);
        });


        menuBt.setOnClickListener(v -> onMenuItemClick());

        floatingActionButton.setOnClickListener(v -> {
            Intent mapIntent = new Intent(DetailsActivity.this, MapsActivity.class);

            startActivity(mapIntent);

        });

        backArrow.setOnClickListener(v -> {
            Intent intent1 = new Intent(DetailsActivity.this, DailyTripsActivity.class);
            startActivity(intent1);

        });
        getTripDetails(tripId);

    }


    private void initView() {
        busDriver = (TextView) findViewById(R.id.bus_driver_tv);
        vehiclePlateNumber = (TextView) findViewById(R.id.bus_number_tv);
        startPoint = (TextView) findViewById(R.id.line_name_tv);
        endPoint = (TextView) findViewById(R.id.supervisor_name_tv);
        vehicleSpeedLimit = (TextView) findViewById(R.id.fast_rout_tv);
        notificationBt = findViewById(R.id.notification_bt);
        menuBt = findViewById(R.id.menu_bt);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        floatingActionButton = findViewById(R.id.gps_fab);
        backArrow = findViewById(R.id.back_arrow_ib);
    }

    private void displayTripDetails(TripDetail tripDetail) {
        busDriver.setText(tripDetail.getDriverName());
        vehiclePlateNumber.setText(tripDetail.getVehiclePlateNumber());
        startPoint.setText(tripDetail.getStartPoint());
        endPoint.setText(tripDetail.getEndPoint());
        vehicleSpeedLimit.setText(tripDetail.getVehicleSpeedLimit());
    }


    private void onMenuItemClick() {
        PopupMenu popup = new PopupMenu(DetailsActivity.this, menuBt);
        //Inflating the Popup using xml file
        popup.getMenuInflater().inflate(R.menu.popup_menu, popup.getMenu());

        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            public boolean onMenuItemClick(MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.menu_item_one:
                        //TODO: handle Log out button
                        break;
                    case R.id.menu_item_two:
                        //TODO: handle about us button
                        break;
                }
                return true;
            }
        });
        popup.show();
    }

    public void getTripDetails(String tripId) {
        Retrofit retrofit = RetrofitInstance.retrofitConnection();
        TripDetailsApi tripDetailsApi = retrofit.create(TripDetailsApi.class);

        Call<TripDetailsModel> getDailyTripModelCall = tripDetailsApi.getTripdetails(tripId);

        getDailyTripModelCall.enqueue(new Callback<TripDetailsModel>() {
            @Override
            public void onResponse(Call<TripDetailsModel> call, Response<TripDetailsModel> response) {

                if (response.isSuccessful() && response.body() != null) {

                    String code = response.body().getStatus().getCode();

                    TripDetail tripDetail = response.body().getTripDetail();
                    if (code.equals("200")) {

                        displayTripDetails(tripDetail);

                    } else if (code.equals("404")) {
                        Snackbar.make(findViewById(R.id.trip_details), "sorry, Trip not found", Snackbar.LENGTH_SHORT).show();
                    } else {
                        Snackbar.make(findViewById(R.id.trip_details), "Internal Error", Snackbar.LENGTH_SHORT).show();
                    }
                } else {
                    Snackbar.make(findViewById(R.id.trip_details), "General Error", Snackbar.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<TripDetailsModel> call, Throwable t) {
                Snackbar.make(findViewById(R.id.trip_details), "Oops! Failed to download data, please check your internet connection", Snackbar.LENGTH_SHORT).show();

            }
        });
    }
}

